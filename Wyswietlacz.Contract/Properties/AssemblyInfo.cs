﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Ogólne informacje o zestawie są kontrolowane poprzez następujący 
// zbiór atrybutów. Zmień wartości tych atrybutów by zmodyfikować informacje
// powiązane z zestawem.
[assembly: AssemblyTitle("Wyswietlacz.Contract")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Wyswietlacz.Contract")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Ustawienie wartości ComVisible na false sprawia, że typy w tym zestawie nie będą widoczne 
// dla składników COM.  Jeśli potrzebny jest dostęp do typu w tym zestawie z 
// COM, ustaw wartość ComVisible na true, dla danego typu.
[assembly: ComVisible(false)]

// Następujący GUID jest dla ID typelib jeśli ten projekt jest dostępny dla COM
[assembly: Guid("5278a540-8f90-481e-9f53-8517a0cf4009")]

// Informacje o wersji zestawu zawierają następujące cztery wartości:
//
//      Wersja główna
//      Wersja pomocnicza 
//      Numer kompilacji
//      Rewizja
//
// Można określać wszystkie wartości lub używać domyślnych numerów kompilacji i poprawki 
// poprzez użycie '*', jak pokazane jest poniżej:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
