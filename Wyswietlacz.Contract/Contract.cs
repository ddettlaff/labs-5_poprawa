﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyswietlacz.Contract
{
    public interface IWyswietlacz
    {
        void NowyTekst(string text);
    }
}
