﻿using System;
using PK.Container;
using System.Reflection;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer contener = new Container();
            contener.Register(typeof(Glowny.Implementation.Glowny));
            contener.Register(typeof(Wyswietlacz.Implementation.Wyswietlacz));
            return contener;
        }
    }
}
