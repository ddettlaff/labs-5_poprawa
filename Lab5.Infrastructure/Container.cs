﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Lab5.Infrastructure
{
    public class Container:IContainer
    {

        IDictionary<Type, Type> containerType;
        IDictionary<Type, Delegate> containerDelegate;
        IDictionary<Type, object> containerObject;

        public Container()
        {
            containerType = new Dictionary<Type, Type>();
            containerDelegate = new Dictionary<Type, Delegate>();
            containerObject = new Dictionary<Type, object>();

        }

        public void Register<T>(Func<T> provider) where T : class
        {
            if (typeof(T).IsInterface)
            {
                if (containerDelegate.ContainsKey(typeof(T)) == false)
                {
                    containerDelegate.Add(typeof(T), provider);
                }
                else
                {
                    containerDelegate[typeof(T)] = provider;
                }
            }
            else
            {
                foreach (var i in typeof(T).GetInterfaces())
                {
                    if (!containerDelegate.ContainsKey(i))
                    {
                        containerDelegate.Add(i, provider);
                    }
                    else
                    {
                        containerDelegate[i] = provider;
                    }
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            if (typeof(T).IsInterface)
            {
                if (containerObject.ContainsKey(typeof(T)) == false)
                {
                    containerObject.Add(typeof(T), impl);
                }
                else
                {
                    containerObject[typeof(T)] = impl;
                }
            }
            else
            {
                foreach (var i in typeof(T).GetInterfaces())
                {
                    if (containerObject.ContainsKey(i) == false)
                    {
                        containerObject.Add(i, impl);
                    }

                    else
                    {
                        containerObject[i] = impl;
                    }
                }
            }
        }

        public void Register(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                if (containerType.ContainsKey(i) == false)
                {
                    containerType.Add(i, type);
                }

                else
                {
                    containerType[i] = type;
                }
            }
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var i in assembly.GetExportedTypes())
            {
                Register(i);
            }
        }

        public object Resolve(Type type)
        {
            if (type.IsInterface)
            {
                if (containerObject.ContainsKey(type))
                {
                    return containerObject[type];
                }
                else if (containerDelegate.ContainsKey(type))
                {
                    return containerDelegate[type].DynamicInvoke();
                }
                else if (containerType.ContainsKey(type))
                {
                    List<object> dataSource = new List<object>();
                    int licznosc = 0;



                    foreach (var i in containerType[type].GetConstructors())
                    {
                        var parms = i.GetParameters();
                        if (parms.Count() > licznosc)
                        {
                            licznosc = parms.Count((x) => (x.ParameterType.IsInterface == true));
                            dataSource.Clear();
                            object resolve = null;
                            foreach (var parameter in parms)
                            {
                                if (parameter.ParameterType.IsInterface)
                                {
                                    resolve = Resolve(parameter.ParameterType);
                                }
                                if (resolve == null)
                                {
                                    throw new UnresolvedDependenciesException();
                                }
                                dataSource.Add(resolve);
                            }
                        }
                    }
                    var instancja = Activator.CreateInstance(containerType[type], dataSource.ToArray());
                    return instancja;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                throw new ArgumentException("Błąd interfejsu!!! :-(");
            }
        }

        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }
    }
}
